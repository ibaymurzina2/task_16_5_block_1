﻿

#include <iostream>
using namespace std;
#include <time.h>

int main()
{
	//локализация языка
	setlocale(LC_ALL, "RUS");
	
	//размерность массива
	const int sizeArray = 8;
	
	//вытаскиваем текущий день и берем остаток от деления дня 
	//на размерность массива, чтобы получить нужную строку массива
	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);
	int stringArray = buf.tm_mday % sizeArray;
	
	int stringSum = 0;
	
	int twoarray[sizeArray][sizeArray];
	for (int i = 0; i < sizeArray; i++)
	{
		for (int j = 0; j < sizeArray; j++)
		{
			 twoarray[i][j] = i+j;
			 cout << twoarray[i][j];	
			 if (stringArray == i)
			 {
				 stringSum += twoarray[i][j];
			 }
			 
		}
		cout << "\n";
	}

	cout << "\n\n" << "Строка с индексом: " << stringArray << "\n";
	cout << "Сумма строки с индексом " << stringArray << " равна " << stringSum << "\n";
	
	//для проверки
	//int test = 31 % 8;
	//cout << "test = " << test;

	return 0;
}

